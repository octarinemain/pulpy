'use strict';

$(document).ready(function () {

    // Rules Swiper
    function rulesSlider() {
        var rulesSlide = $('.rules-swiper_winners');
        rulesSlide = new Swiper('.rules-swiper_winners', {
            slidesPerView: 3,
            spaceBetween: 0,
            allowTouchMove: false,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            breakpoints: {
                768: {
                    allowTouchMove: true,
                    slidesPerView: 1,
                    spaceBetween: 10
                }
            }
        });
    };
    rulesSlider();

    // Cookies
    function cookies() {
        var cookies = $('.cookies');
        var data = sessionStorage.getItem('cookie');
        if (data === null) {
            cookies.addClass('js-show');
        }

        $('.cookies .btn').on('click', function (e) {
            e.preventDefault();
            cookies.removeClass('js-show');
            sessionStorage.setItem('cookie', 'ok');
        });
    }

    cookies();

    // Clamping Footer
    function clampingFooter() {
        var footerW = $('.footer').outerHeight();
        $('.height-helper').css('margin-top', '-' + footerW + 'px');
        $('.content-wrap').css('padding-top', footerW + 'px');
    }

    // PopUp
    function popUp() {
        $('.js-popup-button').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            var popupClass = '.' + $(this).attr('data-popupShow');
            $(popupClass).addClass('js-popup-show');
            $('body').addClass('no-scroll');
        });
        closePopup();
    }

    // Close PopUp
    function closePopup() {
        $('.js-close-popup').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scroll');
        });

        $('.popup').on('click', function (e) {
            var div = $(".popup__wrap");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('js-popup-show');
                $('body').removeClass('no-scroll');
            }
        });
    }
    popUp();

    // Input Password
    $('.js-pass').on('click', function () {
        var input = $(this).closest('div').find('input');
        if (input.attr('type') === 'password') {
            input.attr('type', 'text');
        } else {
            input.attr('type', 'password');
        }
    });

    // Select
    $('select').select2({});
    var winners = $('.form__input_winners');
    winners.on('click', function () {
        var selectWinners = $('body').find('.select2-container .select2-dropdown');
        selectWinners.addClass('select2-dropdown_active');
    });

    // Jquery Validate
    $('form').each(function () {
        $(this).validate({
            ignore: [],
            errorClass: "error",
            validClass: "success",
            errorElement: "div",
            wrapper: "span",
            rules: {
                name: {
                    required: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                surname: {
                    required: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                secondname: {
                    required: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                secondfile: {
                    required: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                file: {
                    required: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                thirdfile: {
                    required: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                phone: {
                    required: true,
                    phone: true
                },
                email: {
                    required: true,
                    email: true,
                    normalizer: function normalizer(value) {
                        return $.trim(value);
                    }
                },
                password: {
                    required: true
                },
                personalAgreement: {
                    required: true
                },
                rules: {
                    required: true
                },
                captcha: {
                    required: true
                }
            },
            messages: {
                phone: {
                    phone: "Укажите номер телефона"
                },
                email: {
                    email: "Введите правильный email"
                }
            }
        });

        jQuery.validator.addMethod("phone", function (value, element) {
            return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
        });
    });

    // Masked Phone
    $("input[type='tel']").mask("+7(999)999-99-99");

    // Scroll
    $('.winners__table').scrollbar();

    // Burger Menu
    function mobMenu() {
        var menu = $('#menu-wrap');
        var btnCloseMenu = menu.find('.close-menu');
        $('#mob-menu').on('click', function () {
            menu.addClass('menu-wrap_active');
            $('.bg-opacity').addClass('bg-opacity_active');

            $('.bg-opacity').on('click', function (e) {
                menu.removeClass('menu-wrap_active');
                $('.bg-opacity').removeClass('bg-opacity_active');
            });
        });
        btnCloseMenu.on('click', function () {
            menu.removeClass('menu-wrap_active');
            $('.bg-opacity').removeClass('bg-opacity_active');
        });
    };
    mobMenu();

    // Download-Img
    function downloadImg() {
        var fileImgFirst = $('#file-first');
        var fileImgSecond = $('#file-second');
        var fileImgThird = $('#file-third');
        var imgFirst = $('#img-first');
        var imgSecond = $('#img-second');
        var imgThird = $('#img-third');
        var btnFirst = $('#btn-img-first');
        var btnSecond = $('#btn-img-second');
        var btnThird = $('#btn-img-third');
        fileImgFirst.change(function () {
            if (this.files[0]) {
                var file = new FileReader();
                file.addEventListener('load', function () {
                    imgFirst.find('.file__text').addClass('file__text_none');
                    imgFirst.css('backgroundImage', "url(" + file.result + ")");
                    btnFirst.removeClass('btn_delete');
                }, false);
                file.readAsDataURL(this.files[0]);
            }
        });
        btnFirst.on('click', function () {
            imgFirst.find('.file__text').removeClass('file__text_none');
            fileImgFirst.val('');
            imgFirst.removeAttr('style');
            btnFirst.addClass('btn_delete');
            return false;
        });
        fileImgSecond.change(function () {
            if (this.files[0]) {
                var file = new FileReader();
                file.addEventListener('load', function () {
                    imgSecond.find('.file__text').addClass('file__text_none');
                    imgSecond.css('backgroundImage', "url(" + file.result + ")");
                    btnSecond.removeClass('btn_delete');
                }, false);
                file.readAsDataURL(this.files[0]);
            }
        });
        btnSecond.on('click', function () {
            imgSecond.find('.file__text').removeClass('file__text_none');
            fileImgSecond.val('');
            imgSecond.removeAttr('style');
            btnSecond.addClass('btn_delete');
            return false;
        });
        fileImgThird.change(function () {
            if (this.files[0]) {
                var file = new FileReader();
                file.addEventListener('load', function () {
                    imgThird.find('.file__text').addClass('file__text_none');
                    imgThird.css('backgroundImage', "url(" + file.result + ")");
                    btnThird.removeClass('btn_delete');
                }, false);
                file.readAsDataURL(this.files[0]);
            }
        });
        btnThird.on('click', function () {
            imgThird.find('.file__text').removeClass('file__text_none');
            fileImgThird.val('');
            imgThird.removeAttr('style');
            btnThird.addClass('btn_delete');
            return false;
        });
    };
    downloadImg();
});